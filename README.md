# A movie-info application build with flutter

The main flow of the application is as following :
Infrastructure <- Domain <- Blocs <- Presentation

where :
- Infrastructure : contains DataSources and Repositories
- Domain : contains Entities and Failures (which use to communicate with bloc)
- Bloc : app / ephemeral state management
- Presentation : contains Pages (or screens) and/or Widgets (smaller UI components)

This Movie app mainly consists of 2 main modules.
## Registration Page / Login Page
<p align="left">
  <img src="https://user-images.githubusercontent.com/89200470/158937127-db39a531-adc1-44de-88ae-19f348aade57.png"
     width="225" height="450" />  
  <img src="https://user-images.githubusercontent.com/89200470/158937115-919e1dea-f9de-457b-a49e-0db05f6edb6e.png"
     width="225" height="450" />   
</p>
Data saved to Local Storage (Sqlite)
User login status saved to Shared Preferences

## Movie Page / Movie Detail page
<p align="left">
  <img src="https://user-images.githubusercontent.com/89200470/158937121-a456c001-1ac9-420f-abbc-24a79075349d.png"
     width="225" height="450" />    
<img src="https://user-images.githubusercontent.com/89200470/158937126-550d95a3-d827-47ec-94a6-80f94d4b1bac.png"
     width="225" height="450" />    
</p>
Shows data from themoviedb API

# Installation
To run this application, simply clone it from this repository :
```bash
git clone https://github.com/hansenbudinata21/clockapp-flutter.git
```

Then run flutter pub get to get all the dependencies :
```bash
flutter pub get
```