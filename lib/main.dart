import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'core/constants/constant.dart';
import 'core/widgets/loading.dart';
import 'features/auth/blocs/auth_bloc_cubit.dart';
import 'features/auth/presentation/pages/login_page.dart';
import 'features/movie/blocs/home_bloc_cubit.dart';
import 'features/movie/presentation/pages/home_bloc_screen.dart';
import 'injection_container.dart' as di;
import 'injection_container.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(ScreenUtilConstants.width, ScreenUtilConstants.height),
      builder: () {
        return BlocProvider(
          create: (context) => sl<AuthBlocCubit>()..fetchHistoryLogin(),
          child: MaterialApp(
            title: 'Flutter Demo',
            theme: ThemeData(
              fontFamily: 'Poppins',
              primarySwatch: Colors.blue,
              visualDensity: VisualDensity.adaptivePlatformDensity,
            ),
            home: BlocBuilder<AuthBlocCubit, AuthBlocState>(builder: (context, state) {
              if (state is AuthBlocInitialState || state is AuthBlocLoadingState) return LoadingIndicator();
              if (state is AuthBlocLoggedInState)
                return BlocProvider(
                  create: (context) => sl<HomeBlocCubit>()..fetchingData(page: 1),
                  child: HomeBlocScreen(),
                );
              return LoginPage();
            }),
          ),
        );
      },
    );
  }
}
