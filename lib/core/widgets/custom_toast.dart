import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../constants/constant.dart';

class CustomToast extends StatelessWidget {
  final IconData iconData;
  final String content;
  final Color color;

  CustomToast({
    required this.iconData,
    required this.content,
    this.color = Colors.blue,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 40.w, vertical: 20.h),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(CurveSize.extraCurve),
        color: color,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(iconData, color: Colors.white),
          SizedBox(width: 30.w),
          Flexible(
            child: Text(
              content,
              style: TextStyle(color: Colors.white, fontSize: FontSize.mediumFont),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
    );
  }
}
