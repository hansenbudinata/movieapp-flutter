import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:majootestcase/core/constants/constant.dart';

class ErrorScreen extends StatelessWidget {
  final String message;
  final Function()? retry;
  final Color? textColor;
  final double fontSize;
  final double gap;
  final Widget? retryButton;
  final IconData? iconData;

  const ErrorScreen({
    Key? key,
    this.gap = 10,
    this.retryButton,
    this.message = "",
    this.fontSize = 14,
    this.retry,
    this.textColor,
    this.iconData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (iconData != null)
              Icon(
                iconData,
                color: Colors.blue,
                size: 200.w,
              ),
            SizedBox(height: 50.h),
            Text(
              message,
              style: TextStyle(
                color: textColor ?? Colors.black,
                fontSize: FontSize.largeFont,
                fontWeight: FontWeight.w400,
              ),
            ),
            retry != null
                ? Column(
                    children: [
                      SizedBox(height: 100.h),
                      retryButton ??
                          IconButton(
                            onPressed: () {
                              if (retry != null) retry!();
                            },
                            icon: Icon(Icons.refresh_sharp),
                          ),
                    ],
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }
}
