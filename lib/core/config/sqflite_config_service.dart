import 'dart:async';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

Future<Database> sqflite() async {
  Database database;
  String path = await getDatabasesPath();
  path = join(path, 'movie_user.db');

  database = await openDatabase(
    path,
    version: 1,
    onCreate: (Database db, int version) async {
      await db.execute('CREATE TABLE user (id INTEGER PRIMARY KEY, email TEXT, user_name TEXT, password TEXT)');
    },
  );

  return database;
}
