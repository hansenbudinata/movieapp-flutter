import 'dart:async';

import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import '../constants/constant.dart';

Future<Dio> dio() async {
  Dio dioInstance;
  var option = BaseOptions(baseUrl: '${Api.BASE_URL}/', connectTimeout: 10000, receiveTimeout: 10000);

  dioInstance = new Dio(option);
  dioInstance.interceptors.add(PrettyDioLogger(requestHeader: true, requestBody: true, responseBody: true, responseHeader: false, error: true, compact: true, maxWidth: 90));
  return dioInstance;
}
