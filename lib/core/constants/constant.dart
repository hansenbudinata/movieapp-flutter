import 'package:flutter_screenutil/flutter_screenutil.dart';

class SharedPreferenceKey {
  static const String IS_LOGGED_IN = "is_logged_in";
  static const String USER_VALUE = "user_value";
}

class Api {
  static const String BASE_URL = "https://api.themoviedb.org/";
  static const String API_KEY = "49043de2a585eb09594b788c47fee75b";
  static const String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500/";
}

class ScreenUtilConstants {
  static const double width = 1080;
  static const double height = 2220;
}

class CurveSize {
  static double smallCurve = 15.r;
  static double mediumCurve = 30.r;
  static double largeCurve = 60.r;

  static double extraCurve = 100.r;
}

class FontSize {
  static double smallFont = 36.sp;
  static double mediumFont = 42.sp;
  static double largeFont = 48.sp;

  static double subHeaderFont = 60.sp;
  static double headerFont = 84.sp;
}
