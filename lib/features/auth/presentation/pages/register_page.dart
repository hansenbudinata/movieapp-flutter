import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../../../core/constants/constant.dart';
import '../../../../core/widgets/custom_button.dart';
import '../../../../core/widgets/custom_toast.dart';
import '../../../../core/widgets/text_form_field.dart';
import '../../../../injection_container.dart';
import '../../../movie/blocs/home_bloc_cubit.dart';
import '../../../movie/presentation/pages/home_bloc_screen.dart';
import '../../blocs/auth_bloc_cubit.dart';
import 'login_page.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<RegisterPage> {
  final _emailController = TextController();
  final _userNameController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;
  late FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: BlocListener<AuthBlocCubit, AuthBlocState>(
          listener: (context, state) async {
            if (state is AuthBlocSuccessState) {
              fToast.showToast(
                child: CustomToast(iconData: Icons.check, content: 'Register Berhasil', color: Colors.green),
                gravity: ToastGravity.BOTTOM,
                toastDuration: Duration(milliseconds: 500),
              );

              await Future.delayed(Duration(milliseconds: 500));
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (_) => BlocProvider(
                    create: (context) => sl<HomeBlocCubit>()..fetchingData(page: 1),
                    child: HomeBlocScreen(),
                  ),
                ),
              );
            } else if (state is AuthBlocErrorState) {
              fToast.showToast(
                child: CustomToast(iconData: Icons.close, content: state.error.errorMessage, color: Colors.red),
                gravity: ToastGravity.BOTTOM,
                toastDuration: Duration(seconds: 2),
              );
            }
          },
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 75.w, vertical: 75.h),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Register Akun',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: FontSize.headerFont,
                      fontWeight: FontWeight.bold,
                      // color: colorBlue,
                    ),
                  ),
                  Text(
                    'Silahkan mendaftar terlebih dahulu',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: FontSize.mediumFont,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  SizedBox(height: 50.h),
                  _form(),
                  SizedBox(height: 100.h),
                  Center(
                    child: CustomButton(
                      text: 'Register',
                      onPressed: handleRegister,
                    ),
                  ),
                  SizedBox(height: 50.h),
                  _login(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null) return pattern.hasMatch(val) ? null : 'Masukkan e-mail yang valid.';
              return null;
            },
          ),
          SizedBox(height: 50.h),
          CustomTextFormField(
            context: context,
            controller: _userNameController,
            hint: 'jimmy123',
            label: 'User Name',
          ),
          SizedBox(height: 50.h),
          CustomTextFormField(
            context: context,
            hint: 'password',
            label: 'Password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword ? Icons.visibility_off_outlined : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _login() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => LoginPage(),
            ),
          );
        },
        child: RichText(
          text: TextSpan(
            text: 'Sudah punya akun? ',
            style: TextStyle(color: Colors.black45, fontFamily: 'Poppins'),
            children: [
              TextSpan(
                text: 'Login',
                style: TextStyle(color: Colors.black45, fontFamily: 'Poppins'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void handleRegister() async {
    FocusScope.of(context).unfocus();
    await Future.delayed(Duration(milliseconds: 500));
    final _email = _emailController.value;
    final _userName = _userNameController.value;
    final _password = _passwordController.value;
    if (formKey.currentState?.validate() == true && _email != null && _password != null && _userName != null) {
      await BlocProvider.of<AuthBlocCubit>(context).registerUser(email: _email, userName: _userName, password: _password);
    }
  }
}
