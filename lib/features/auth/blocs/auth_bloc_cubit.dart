import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../domain/entities/user.dart';
import '../domain/failures/auth_failure.dart';
import '../infrastructure/repositories/auth_repository.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  final AuthRepository authRepository;

  AuthBlocCubit({required this.authRepository}) : super(AuthBlocInitialState());

  Future<void> fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    Either<AuthFailure, User?> result = await authRepository.getLoggedInUser();
    result.fold(
      (AuthFailure failure) => emit(AuthBlocErrorState(failure)),
      (User? user) {
        if (user != null) {
          emit(AuthBlocLoggedInState());
        } else {
          emit(AuthBlocLoginState());
        }
      },
    );
  }

  Future<void> loginUser({required String email, required String password}) async {
    emit(AuthBlocLoadingState());
    Either<AuthFailure, void> result = await authRepository.login(email: email, password: password);
    result.fold(
      (AuthFailure failure) {
        emit(AuthBlocErrorState(failure));
      },
      (_) {
        emit(AuthBlocLoggedInState());
      },
    );
  }

  Future<void> registerUser({required String email, required String userName, required String password}) async {
    emit(AuthBlocLoadingState());
    Either<AuthFailure, void> result = await authRepository.register(email: email, password: password, userName: userName);
    result.fold(
      (AuthFailure failure) {
        emit(AuthBlocErrorState(failure));
      },
      (_) {
        emit(AuthBlocSuccessState());
      },
    );
  }

  Future<void> logout() async {
    await authRepository.logout();
    emit(AuthBlocInitialState());
  }
}
