import 'package:dartz/dartz.dart';
import 'package:sqflite/sqflite.dart';

import '../../../../core/errors/exception.dart';
import '../../domain/entities/user.dart';
import '../../domain/failures/auth_failure.dart';
import '../datasources/auth_local_data_source.dart';

abstract class AuthRepository {
  Future<Either<AuthFailure, User>> login({required String email, required String password});
  Future<Either<AuthFailure, void>> register({required String email, required String password, required String userName});
  Future<Either<AuthFailure, User?>> getLoggedInUser();
  Future<void> logout();
}

class AuthRepositoryImpl implements AuthRepository {
  final AuthLocalDataSource authLocalDataSource;
  AuthRepositoryImpl({required this.authLocalDataSource});

  @override
  Future<Either<AuthFailure, User>> login({required String email, required String password}) async {
    try {
      User user = await authLocalDataSource.login(email: email, password: password);
      return Right(user);
    } on DatabaseException {
      return Left(AuthFailure(errorType: ErrorType.databaseError, errorMessage: "Something went wrong with the database."));
    } on NoDataException {
      return Left(AuthFailure(errorType: ErrorType.invalidCombination, errorMessage: "Login gagal, periksa kembali inputan anda."));
    }
  }

  @override
  Future<Either<AuthFailure, void>> register({required String email, required String password, required String userName}) async {
    try {
      await authLocalDataSource.register(email: email, password: password, userName: userName);
      return Right(null);
    } on DatabaseException {
      return Left(AuthFailure(errorType: ErrorType.databaseError, errorMessage: "Something went wrong with the database."));
    } on AlreadyExistException {
      return Left(AuthFailure(errorType: ErrorType.emailAlreadyUsed, errorMessage: "Email already used."));
    }
  }

  @override
  Future<Either<AuthFailure, User?>> getLoggedInUser() async {
    try {
      User? loginStatus = await authLocalDataSource.getLoggedInUser();
      return Right(loginStatus);
    } catch (e) {
      return Left(AuthFailure(errorType: ErrorType.databaseError, errorMessage: "Something went wrong with the database."));
    }
  }

  @override
  Future<void> logout() async {
    await authLocalDataSource.logout();
  }
}
