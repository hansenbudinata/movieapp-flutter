import 'dart:convert';

import '../../../../core/constants/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

import '../../../../core/errors/exception.dart';
import '../../domain/entities/user.dart';

abstract class AuthLocalDataSource {
  Future<User> login({required String email, required String password});
  Future<void> register({required String email, String? userName, required String password});
  Future<User?> getLoggedInUser();
  Future<void> logout();
}

class AuthLocalDataSourceImpl implements AuthLocalDataSource {
  final Database database;
  final SharedPreferences sharedPreferences;
  AuthLocalDataSourceImpl({required this.database, required this.sharedPreferences});

  @override
  Future<User> login({required String email, required String password}) async {
    List<Map<String, dynamic>> result = await database.rawQuery('SELECT * FROM user WHERE email = ? AND password = ?', [email, password]);
    if (result.length <= 0) throw NoDataException();

    User user = User.fromJson(result[0]);

    sharedPreferences.setBool(SharedPreferenceKey.IS_LOGGED_IN, true);
    sharedPreferences.setString(SharedPreferenceKey.USER_VALUE, jsonEncode(user.toJson()));
    return user;
  }

  @override
  Future<void> register({required String email, String? userName, required String password}) async {
    List<Map<String, dynamic>> result = await database.rawQuery('SELECT * FROM user WHERE email = ?', [email]);
    if (result.length > 0) throw AlreadyExistException();

    int id = await database.rawInsert('INSERT INTO user (email, user_name, password) VALUES (?, ?, ?)', [email, userName, password]);
    if (id > 0) {
      sharedPreferences.setBool(SharedPreferenceKey.IS_LOGGED_IN, true);
      sharedPreferences.setString(SharedPreferenceKey.USER_VALUE, jsonEncode(User(email: email, userName: userName, password: password).toJson()));
    } else {
      throw DatabaseException;
    }
  }

  Future<User?> getLoggedInUser() async {
    bool loginStatus = sharedPreferences.getBool(SharedPreferenceKey.IS_LOGGED_IN) ?? false;
    if (loginStatus) {
      String? userString = sharedPreferences.getString(SharedPreferenceKey.USER_VALUE);
      if (userString != null) {
        User user = User.fromJson(jsonDecode(userString));
        return Future.value(user);
      } else {
        return Future.value(null);
      }
    } else {
      return Future.value(null);
    }
  }

  @override
  Future<void> logout() async {
    sharedPreferences.setBool(SharedPreferenceKey.IS_LOGGED_IN, false);
    sharedPreferences.remove(SharedPreferenceKey.USER_VALUE);
  }
}
