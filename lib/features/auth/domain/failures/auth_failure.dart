import '../../../../core/errors/failure.dart';

enum ErrorType {
  databaseError,
  emailAlreadyUsed,
  invalidCombination,
}

class AuthFailure extends Failure {
  final ErrorType errorType;
  final String errorMessage;
  AuthFailure({required this.errorType, required this.errorMessage});

  @override
  List<Object?> get props => [errorType, errorMessage];

  @override
  String get failureMessage => errorMessage;
}
