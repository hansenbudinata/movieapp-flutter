class User {
  String email;
  String? userName;
  String password;

  User({
    required this.email,
    this.userName,
    required this.password,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      email: json['email'],
      password: json['password'],
      userName: json['username'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'email': email,
      'password': password,
      'username': userName,
    };
  }
}
