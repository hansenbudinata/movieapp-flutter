import 'package:flutter/material.dart';

import '../../../../core/errors/failure.dart';

enum ErrorType {
  httpError,
  internetConnectionError,
}

class MovieFailure extends Failure {
  final ErrorType errorType;
  final String errorMessage;
  final IconData errorIcon;
  MovieFailure({required this.errorType, required this.errorMessage, required this.errorIcon});

  @override
  List<Object?> get props => [errorType, errorMessage];

  @override
  String get failureMessage => errorMessage;
}
