class Movie {
  int page;
  List<Results> results;
  int totalPages;
  int totalResults;

  Movie({
    required this.page,
    required this.results,
    required this.totalPages,
    required this.totalResults,
  });

  factory Movie.fromJson(Map<String, dynamic> json) {
    return Movie(
      page: json['page'],
      results: json['results'] == null ? [] : json['results'].map<Results>((v) => Results.fromJson(v)).toList(),
      totalPages: json['total_pages'],
      totalResults: json['total_results'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'page': this.page,
      'results': results.map((v) => v.toJson()).toList(),
      'total_pages': totalPages,
      'total_results': totalResults,
    };
  }
}

class Results {
  double voteAverage;
  String overview;
  DateTime? releaseDate;
  bool? adult;
  String? backdropPath;
  int voteCount;
  List<int> genreIds;
  int id;
  String originalLanguage;
  String? originalTitle;
  String posterPath;
  String? title;
  bool? video;
  double popularity;
  String mediaType;
  String? name;
  String? originalName;
  List<String>? originCountry;
  String? firstAirDate;

  Results({
    required this.voteAverage,
    required this.overview,
    required this.releaseDate,
    required this.adult,
    required this.backdropPath,
    required this.voteCount,
    required this.genreIds,
    required this.id,
    required this.originalLanguage,
    required this.originalTitle,
    required this.posterPath,
    required this.title,
    required this.video,
    required this.popularity,
    required this.mediaType,
    required this.name,
    required this.originalName,
    required this.originCountry,
    required this.firstAirDate,
  });

  factory Results.fromJson(Map<String, dynamic> json) {
    return Results(
      voteAverage: json['vote_average'],
      overview: json['overview'],
      releaseDate: json['release_date'] != null ? DateTime.parse(json['release_date']) : null,
      adult: json['adult'],
      backdropPath: json['backdrop_path'],
      voteCount: json['vote_count'],
      genreIds: json['genre_ids'].cast<int>(),
      id: json['id'],
      originalLanguage: json['original_language'],
      originalTitle: json['original_title'],
      posterPath: json['poster_path'],
      title: json['title'],
      video: json['video'],
      popularity: json['popularity'],
      mediaType: json['media_type'],
      name: json['name'],
      originalName: json['original_name'],
      originCountry: json['origin_country']?.cast<String>(),
      firstAirDate: json['first_air_date'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'vote_average': this.voteAverage,
      'overview': this.overview,
      'release_date': this.releaseDate?.toIso8601String(),
      'adult': this.adult,
      'backdrop_path': this.backdropPath,
      'vote_count': this.voteCount,
      'genre_ids': this.genreIds,
      'id': this.id,
      'original_language': this.originalLanguage,
      'original_title': this.originalTitle,
      'poster_path': this.posterPath,
      'title': this.title,
      'video': this.video,
      'popularity': this.popularity,
      'media_type': this.mediaType,
      'name': this.name,
      'original_name': this.originalName,
      'origin_country': this.originCountry,
      'first_air_date': this.firstAirDate,
    };
  }

  static String getGenreName(int id) {
    Map<int, String> genres = {
      28: "Action",
      12: "Adventure",
      16: "Animation",
      35: "Comedy",
      80: "Crime",
      99: "Documentary",
      18: "Drama",
      10751: "Family",
      14: "Fantasy",
      36: "History",
      27: "Horror",
      10402: "Music",
      9648: "Mystery",
      10749: "Romance",
      878: "Science Fiction",
      10770: "TV Movie",
      53: "Thriller",
      10752: "War",
      37: "Western",
    };

    return genres[id] ?? "";
  }

  String get genreString {
    String result = genreIds.map((e) => getGenreName(e)).toList().toString();
    return result.substring(1, result.length - 1);
  }
}
