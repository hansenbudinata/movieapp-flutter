import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/widgets/custom_button.dart';
import '../../../../core/widgets/error_screen.dart';
import '../../../../core/widgets/loading.dart';
import '../../blocs/home_bloc_cubit.dart';
import 'home_bloc_loaded_screen.dart';

class HomeBlocScreen extends StatelessWidget {
  const HomeBlocScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBlocCubit, HomeBlocState>(
      builder: (context, state) {
        if (state is HomeBlocLoadedState) {
          return HomeBlocLoadedScreen(data: state.data, page: state.page);
        } else if (state is HomeBlocLoadingState) {
          return LoadingIndicator();
        } else if (state is HomeBlocInitialState) {
          return Scaffold();
        } else if (state is HomeBlocErrorState) {
          return ErrorScreen(
            message: state.error.failureMessage,
            iconData: state.error.errorIcon,
            retry: () {},
            retryButton: CustomButton(
              onPressed: () {
                BlocProvider.of<HomeBlocCubit>(context).fetchingData(page: 1);
              },
              text: "Refresh",
            ),
          );
        }

        return Center(child: Text(kDebugMode ? "State not implemented $state" : ""));
      },
    );
  }
}
