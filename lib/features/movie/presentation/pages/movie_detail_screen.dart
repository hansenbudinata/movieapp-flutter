import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../core/constants/constant.dart';
import '../../domain/entities/movie.dart';

class MovieDetailScreen extends StatelessWidget {
  final Results data;

  const MovieDetailScreen({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Stack(
            children: [
              SizedBox(
                width: 1.sw,
                height: 0.8.sh,
                child: CachedNetworkImage(
                  imageUrl: Api.IMAGE_BASE_URL + data.posterPath,
                  fit: BoxFit.cover,
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
                    ),
                  ),
                  placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                ),
              ),
              Container(
                height: 1.sh - ScreenUtil().statusBarHeight,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [Colors.transparent, Theme.of(context).scaffoldBackgroundColor],
                    stops: [0.2, 0.65],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10.w),
                child: BackButton(color: Colors.white),
              ),
              Container(
                margin: EdgeInsets.only(top: 0.55.sh),
                padding: EdgeInsets.symmetric(horizontal: 50.w),
                child: Column(
                  children: [
                    Text(
                      data.name ?? data.originalTitle ?? '',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: FontSize.headerFont,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 25.h),
                    Text(
                      '${data.releaseDate?.year ?? '-'}  |  ${data.genreString}',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: FontSize.mediumFont,
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 25.h),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          data.voteAverage.toString(),
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: FontSize.mediumFont,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(width: 20.w),
                        RatingBarIndicator(
                          rating: data.voteAverage,
                          itemBuilder: (context, index) => Icon(Icons.star, color: Colors.amber),
                          itemCount: 10,
                          itemSize: 50.h,
                        ),
                      ],
                    ),
                    SizedBox(height: 50.h),
                    Text(
                      data.overview,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: FontSize.smallFont,
                      ),
                      textAlign: TextAlign.justify,
                    ),
                    SizedBox(height: 50.h),
                    Text(
                      'Popularity Score : ${data.popularity}',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: FontSize.mediumFont,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
