import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../core/constants/constant.dart';
import '../../../auth/blocs/auth_bloc_cubit.dart';
import '../../../auth/presentation/pages/login_page.dart';
import '../../blocs/home_bloc_cubit.dart';
import '../../domain/entities/movie.dart';
import 'movie_detail_screen.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Results> data;
  final int page;

  const HomeBlocLoadedScreen({
    Key? key,
    required this.data,
    required this.page,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 50.w, vertical: 50.h),
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Column(
              children: [
                Row(
                  children: [
                    SizedBox(width: 100.w),
                    Expanded(
                      child: Text(
                        'Trending Movie',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: FontSize.subHeaderFont,
                          fontWeight: FontWeight.bold,
                        ),
                        maxLines: 1,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    IconButton(
                      icon: Icon(Icons.logout, size: 100.w),
                      onPressed: () async {
                        await BlocProvider.of<AuthBlocCubit>(context).logout();
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(builder: (context) => LoginPage()),
                        );
                      },
                    ),
                  ],
                ),
                SizedBox(height: 100.h),
                GridView.builder(
                  itemCount: data.length,
                  itemBuilder: (context, index) {
                    return movieItemWidget(context, data[index]);
                  },
                  padding: EdgeInsets.symmetric(horizontal: 25.w),
                  primary: false,
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 0.6,
                    crossAxisSpacing: 40.w,
                    mainAxisSpacing: 40.h,
                  ),
                ),
                SizedBox(height: 100.h),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    if (page > 1)
                      GestureDetector(
                        onTap: () {
                          BlocProvider.of<HomeBlocCubit>(context).fetchingData(page: page - 1);
                        },
                        child: _buildPageItem(color: Colors.grey.withAlpha(100), page: page - 1),
                      ),
                    SizedBox(width: 25.w),
                    _buildPageItem(color: Colors.blue.withAlpha(150), page: page),
                    SizedBox(width: 25.w),
                    if (page < 20)
                      GestureDetector(
                        onTap: () {
                          BlocProvider.of<HomeBlocCubit>(context).fetchingData(page: page + 1);
                        },
                        child: _buildPageItem(color: Colors.grey.withAlpha(100), page: page + 1),
                      ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget movieItemWidget(BuildContext context, Results data) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) {
              return MovieDetailScreen(data: data);
            },
          ),
        );
      },
      child: Stack(
        children: [
          CachedNetworkImage(
            imageUrl: Api.IMAGE_BASE_URL + data.posterPath,
            fit: BoxFit.cover,
            imageBuilder: (context, imageProvider) => Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(CurveSize.mediumCurve),
                image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
              ),
            ),
            placeholder: (context, url) => Center(child: CircularProgressIndicator()),
            errorWidget: (context, url, error) => const Icon(Icons.error),
          ),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(CurveSize.mediumCurve),
              gradient: const LinearGradient(
                colors: [Colors.transparent, Colors.black],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                stops: [0.65, 1],
              ),
            ),
          ),
          Positioned(
            left: 0,
            bottom: 0,
            child: Container(
              width: (1.sw - 190.w) / 2,
              padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
              child: Text(
                data.name ?? data.originalTitle ?? '',
                style: TextStyle(color: Colors.white),
                maxLines: 4,
                textAlign: TextAlign.left,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Container _buildPageItem({required Color color, required int page}) {
    return Container(
      height: 100.w,
      width: 100.w,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(CurveSize.smallCurve), color: color),
      child: Center(child: Text('$page', style: TextStyle(color: Colors.black, fontSize: FontSize.mediumFont, fontWeight: FontWeight.bold))),
    );
  }
}
