part of 'home_bloc_cubit.dart';

abstract class HomeBlocState extends Equatable {
  const HomeBlocState();

  @override
  List<Object> get props => [];
}

class HomeBlocInitialState extends HomeBlocState {}

class HomeBlocLoadingState extends HomeBlocState {}

class HomeBlocLoadedState extends HomeBlocState {
  final List<Results> data;
  final int page;
  HomeBlocLoadedState(this.data, this.page);
  @override
  List<Object> get props => [data];
}

class HomeBlocErrorState extends HomeBlocState {
  final MovieFailure error;

  HomeBlocErrorState(this.error);

  @override
  List<Object> get props => [error];
}
