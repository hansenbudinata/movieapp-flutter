import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import '../infrastructure/repositories/movie_repository.dart';
import '../domain/entities/movie.dart';
import '../domain/failures/movie_failure.dart';

// import '../../../data/datasources/api_service.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  final MovieRepository movieRepository;

  HomeBlocCubit({required this.movieRepository}) : super(HomeBlocInitialState());

  void fetchingData({required int page}) async {
    emit(HomeBlocLoadingState());
    Either<MovieFailure, Movie> result = await movieRepository.getMovie(page: page);
    result.fold(
      (MovieFailure failure) {
        emit(HomeBlocErrorState(failure));
      },
      (Movie movie) {
        emit(HomeBlocLoadedState(movie.results, page));
      },
    );
  }
}
