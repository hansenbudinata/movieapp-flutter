import 'dart:async';

import 'package:dio/dio.dart';
import '../../../../core/constants/constant.dart';

import '../../domain/entities/movie.dart';

abstract class MovieRemoteDataSource {
  Future<Movie> getMovie({required int page});
}

class MovieRemoteDataSourceImpl implements MovieRemoteDataSource {
  final Dio dio;
  MovieRemoteDataSourceImpl({required this.dio});

  @override
  Future<Movie> getMovie({required int page}) async {
    Response response = await dio.get("/3/trending/all/day?api_key=${Api.API_KEY}&page=$page");
    Map<String, dynamic> result = response.data;
    Movie movie = Movie.fromJson(result);
    return movie;
  }
}
