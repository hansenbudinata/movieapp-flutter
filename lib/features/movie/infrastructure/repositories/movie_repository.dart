import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../../../../core/network/network_info.dart';
import '../../domain/entities/movie.dart';
import '../../domain/failures/movie_failure.dart';
import '../datasources/movie_remote_data_source.dart';

abstract class MovieRepository {
  Future<Either<MovieFailure, Movie>> getMovie({required int page});
}

class MovieRepositoryImpl implements MovieRepository {
  final NetworkInfo networkInfo;
  final MovieRemoteDataSource movieRemoteDataSource;
  MovieRepositoryImpl({required this.movieRemoteDataSource, required this.networkInfo});

  @override
  Future<Either<MovieFailure, Movie>> getMovie({required int page}) async {
    bool connected = await networkInfo.isConnected;
    if (connected) {
      try {
        Movie movie = await movieRemoteDataSource.getMovie(page: page);
        return Right(movie);
      } on DioError catch (e) {
        return Left(MovieFailure(errorType: ErrorType.httpError, errorMessage: e.message, errorIcon: Icons.error));
      }
    } else {
      return Left(MovieFailure(errorType: ErrorType.internetConnectionError, errorMessage: "Internet Connection not available.", errorIcon: Icons.signal_wifi_connected_no_internet_4));
    }
  }
}
