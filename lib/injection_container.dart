import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

import 'core/config/dio_config_service.dart' as dioConfig;
import 'core/config/sqflite_config_service.dart' as sqfliteConfig;
import 'core/network/network_info.dart';
import 'features/auth/blocs/auth_bloc_cubit.dart';
import 'features/auth/infrastructure/datasources/auth_local_data_source.dart';
import 'features/auth/infrastructure/repositories/auth_repository.dart';
import 'features/movie/blocs/home_bloc_cubit.dart';
import 'features/movie/infrastructure/datasources/movie_remote_data_source.dart';
import 'features/movie/infrastructure/repositories/movie_repository.dart';

final GetIt sl = GetIt.instance;

Future<void> init() async {
  sl.registerFactory<AuthBlocCubit>(() => AuthBlocCubit(authRepository: sl()));
  sl.registerFactory<HomeBlocCubit>(() => HomeBlocCubit(movieRepository: sl()));

  sl.registerLazySingleton<AuthRepository>(
    () => AuthRepositoryImpl(
      authLocalDataSource: sl(),
    ),
  );
  sl.registerLazySingleton<MovieRepository>(
    () => MovieRepositoryImpl(
      networkInfo: sl(),
      movieRemoteDataSource: sl(),
    ),
  );

  sl.registerLazySingleton<AuthLocalDataSource>(
    () => AuthLocalDataSourceImpl(
      database: sl(),
      sharedPreferences: sl(),
    ),
  );
  sl.registerLazySingleton<MovieRemoteDataSource>(
    () => MovieRemoteDataSourceImpl(
      dio: sl(),
    ),
  );

  final Database database = await sqfliteConfig.sqflite();
  sl.registerLazySingleton<Database>(() => database);

  final SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton<SharedPreferences>(() => sharedPreferences);

  final Dio dio = await dioConfig.dio();
  sl.registerLazySingleton<Dio>(() => dio);

  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

  sl.registerLazySingleton(() => InternetConnectionChecker());
}
